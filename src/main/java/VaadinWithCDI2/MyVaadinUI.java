package VaadinWithCDI2;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.rapidpm.demo.cdi.commons.logger.CDILogger;
import org.rapidpm.module.se.commons.logger.Logger;

import javax.inject.Inject;

@Theme("mytheme")
@SuppressWarnings("serial")
@CDIUI
public class MyVaadinUI extends UI
{
    /*
    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "VaadinWithCDI2.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }
    */
    @Inject
    @CDILogger
    private Logger logger;

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        final Button button = new Button("Click Me");
        button.addClickListener(new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                layout.addComponent(new Label("Thank you for clicking"));
                logger.warn("Button was clicked");
            }
        });
        configureLayout(layout, button); //Only configuring layouting-issues
    }

    private void configureLayout(final VerticalLayout layout, final Button button) {
        layout.setMargin(true);
        layout.setWidth("100%");
        layout.addComponent(button);
        setContent(layout);
    }

}
